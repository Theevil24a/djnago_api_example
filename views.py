# -*- coding: utf-8 -*-
import json

from django.shortcuts import get_list_or_404
from django.http import HttpResponse
from django.contrib.auth.models import UserManager
from django.db import IntegrityError
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.views.generic import ListView, View

from rest_framework import parsers, renderers
from rest_framework import generics
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import mixins
from rest_framework.response import Response
from rest_framework.decorators import detail_route
from rest_framework.views import APIView
from rest_framework.compat import coreapi, coreschema
from rest_framework.schemas import ManualSchema

from rest_framework.authtoken.models import Token

class UserControl(APIView):
    permission_classes = (
        permissions.AllowAny,
    )

    def get(self, request, *args, **kwargs):
        return HttpResponse(json.dumps(json.dumps(Usuario.objects.all())), content_type='application/json', status=200)


    def post(self, request, *args, **kwargs):
        if request.data.get('delete') == "true":
            try:
                usuario = Usuario.objects.get(id = request.data['id'])
                user = User.objects.get(id = usuario.user.id)

                if (user.is_superuser is not True):
                    user.delete()

                usuario.delete()

                return HttpResponse(json.dumps({"context": True}), content_type='application/json', status=200)
            except User.DoesNotExist: 
                return HttpResponse(json.dumps({"context": False}), content_type='application/json', status=400)
        else:
            try:
                user = User(
                    username = request.data['email'],
                    password = make_password(request.data['password']),
                ).save()
                user_id = User.objects.get( username = request.data['email'])
                usuario_proyecto = Usuario(
                    user = user_id,
                    nombre = request.data['username'],
                    apellido = request.data['apellido'],
                    numerodocumento =  request.data['document'],
                    telefono = request.data['cellphone'],
                    contrasena = request.data['password'],
                    correo = request.data['email']
                ).save()
                
                return HttpResponse(json.dumps({"context": True}), content_type='application/json', status=200)
            except Exception as error: 
                print (error)
                return HttpResponse(json.dumps({"context": False }), content_type='application/json', status=400)
